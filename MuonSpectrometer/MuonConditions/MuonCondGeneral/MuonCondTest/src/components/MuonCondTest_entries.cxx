/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#include "../CSCConditionsTestAlgMT.h"
#include "../MDTConditionsTestAlgMT.h"
#include "../NswCondTestAlg.h"
#include "../NswDcsTestAlg.h"
#include "../NswPassivationTestAlg.h"
#include "../MdtCablingTestAlg.h"
#include "../RpcCablingTestAlg.h"
#include "../MMCablingTestAlg.h"
#include "../MdtCalibFormatAlgTest.h"


DECLARE_COMPONENT(MDTConditionsTestAlgMT)
DECLARE_COMPONENT(CSCConditionsTestAlgMT)
DECLARE_COMPONENT(NswCondTestAlg)
DECLARE_COMPONENT(NswDcsTestAlg)
DECLARE_COMPONENT(NswPassivationTestAlg)
DECLARE_COMPONENT(MdtCablingTestAlg)
DECLARE_COMPONENT(RpcCablingTestAlg)
DECLARE_COMPONENT(MdtCalibFormatAlgTest)
DECLARE_COMPONENT(MMCablingTestAlg)
